package main

import (
	"fmt"
	"os"
	"bufio"
	"net/http"
	"io/ioutil"
	"strings"
	"net/url"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	fmt.Print("Run\n");
	checkingUrl := "https://lj-top.ru/?ipc=yes";
	dbHost := "lj-top.ru";
	dbUser := "proxy";
	dbPassword := "2912";
	dbName := "proxy";
	fmt.Printf("start checking ip addresses\n")
	file, err := os.OpenFile("/sites/go/proxies/misc/ips.txt", os.O_RDWR, 0777)

	if err != nil {
		fmt.Print(err, "\n")
		return
	}

	db, connectionError := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", dbUser, dbPassword, dbHost, dbName))

	err = db.Ping()

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	if connectionError != nil {
		panic(connectionError)
		return
	}

	scanner := bufio.NewScanner(file)

	response, errCurl := http.Get(checkingUrl)

	body, errReading := ioutil.ReadAll(response.Body)
	if errReading != nil {
		fmt.Print(errReading, "\n")
		return
	}

	myIp := strings.Split(string(body), " ")[0]
	fmt.Print(myIp, " is my ip address\n")

	if errCurl != nil {
		fmt.Print(err)
		return
	}

	if err != nil {
		fmt.Print(err)
		return
	}

	chain := make(chan int)

	i := 0
	for scanner.Scan() {
		i = i + 1
		go checkIp(scanner.Text(), myIp, checkingUrl, chain, db)
	}

	j := 0
	for j < i {
		<-chain
		j++
		fmt.Printf("%d from %d finished\n", j, i)
	}

	fmt.Printf("Done\n");
}

func checkIp(proxyIp string, myIp string, checkingUrl string, chain chan int, db *sql.DB) {
	fmt.Printf("Checking ip %s\n", proxyIp)
	status := 0;
	stmt, err := db.Prepare("INSERT INTO proxies SET address=?, status=? ON DUPLICATE KEY UPDATE status=?")
	if err != nil {
		panic(err)
		return
	}

	proxyUrl, errParsing := url.Parse("http://" + proxyIp)

	if errParsing != nil {
		fmt.Printf("%s\n", errParsing)
		stmt.Exec(proxyIp, 0, 0)
		chain <- 1
		return
	}

	myClient := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}

	response, err := myClient.Get(checkingUrl)
	if err != nil {
		fmt.Print(err, "\n")
		stmt.Exec(proxyIp, 0, 0)
		chain <- 1
		return
	}

	body, errReading := ioutil.ReadAll(response.Body)

	if errReading != nil {
		fmt.Print(errReading, "\n")
		stmt.Exec(proxyIp, 0, 0)
		chain <- 1
		return
	}

	realIp := strings.Split(string(body), " ")[0]

	fmt.Printf("Got REMOTE_ADDR=%s using proxy %s\n", realIp, proxyIp)

	if (realIp != myIp) {
		status = 1;
		fmt.Printf("Good proxy: %s\n", proxyIp)
	} else {
		status = 0;
		fmt.Printf("Bad proxy: %s\n", proxyIp)
	}
	stmt.Exec(proxyIp, status, status)

	chain <- 1
}
